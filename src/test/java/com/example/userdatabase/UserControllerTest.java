package com.example.userdatabase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    //Testing get all users
    @Test
    public void getUsersTest(){
        when(userRepository.findAll()).thenReturn(Stream.of
                (new User("amanda", "amanda@gmail.com", "91118222")).collect(Collectors.toList()));
        assertEquals(1, userService.getAll().size());
    }

    //Get user by number
    @Test
    public void getUserbyNumber() {
        String number = "91118222";
        User user = new User("amanda", "amanda@gmail.com", "91118222");
        when(userRepository.findByNumber(number))
                .thenReturn(user);
        assertEquals("91118222", userService.getByNumber(number).getNumber());
    }

    //Creating a new user
    @Test
    public void createUserTest(){
        User user = new User("amanda", "amanda@gmail.com", "91118222");
        when(userRepository.save(user)).thenReturn(user);
        assertEquals(user, userService.create(user));
    }

    //Deleting a user
    @Test
    public void deleteUserTest(){
        User user = new User("amanda", "amanda@gmail.com", "91118222");
        userService.delete(user.getNumber());
        assertEquals(null, userService.getByNumber(user.getNumber()));
    }

    //Deleting all users
    @Test
    public void deleteAllUsersTest(){
        User user = new User("amanda", "amanda@gmail.com", "91118222");
        userService.deleteAll();
        assertEquals(0, userService.getAll().size());
    }

    /*
    //Updating a user by number
    @Test
    public void updateUserbyNumber(){
        User user = new User("amanda", "amanda@gmail.com", "11112222");
        String updatedNumber = "12345678";
        when(userService.update("amanda", "amanda@gmail.com", updatedNumber)).thenReturn(user);
        assertEquals(user, userService.getByNumber(updatedNumber));
    }*/
}
