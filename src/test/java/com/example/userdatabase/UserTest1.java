package com.example.userdatabase;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest1 {

    @Test
    public void user_newUserCreated_correctParamsInitialised() {
        User newUser = new User("Anna", "anna@hotmail.com","90123456");
        assertAll(
                () -> assertEquals("Anna", newUser.getName()),
                () -> assertEquals("anna@hotmail.com", newUser.getEmail()),
                () -> assertEquals("90123456", newUser.getNumber())
        );

    }

    @Test
    public void user_newUserCreated_apiCreated(){
        User newUser = new User("Anna", "anna@hotmail.com","90123456");
        assertTrue(!newUser.getApiKey().isEmpty());
    }

    @Test
    public void user_updateUserInfo_nameChanged(){
        User newUser = new User("Anna", "anna@hotmail.com","90123456");
        newUser.setName("Amanda");
        assertEquals("Amanda", newUser.getName());
    }

    @Test
    public void user_updateUserInfo_emailChanged(){
        User newUser = new User("Anna", "anna@hotmail.com","90123456");
        newUser.setEmail("anna@gmail.com");
        assertEquals("anna@gmail.com", newUser.getEmail());
    }

    @Test
    public void user_updateUserInfo_numberChanged(){
        User newUser = new User("Anna", "anna@hotmail.com","90123456");
        newUser.setNumber("91111111");
        assertEquals("91111111", newUser.getNumber());
    }

    @Test
    public void user_apiGenerationFailed_exceptionOccurs(){
        try {
            User newUser = new User("Anna", "anna@hotmail.com","90123456");
            // Api key generated upon creation of new user
        } catch (AssertionError err){
            fail("Failed to create API key: ");
        }
    }



}
