package com.example.userdatabase.data;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class YahooDataServiceTests {

    private YahooDataService dataService;

    @Before
    public void setUp_validApiKeyHostUrl() {
        dataService = new YahooDataService();
        ReflectionTestUtils.setField(dataService, "HOST", "apidojo-yahoo-finance-v1.p.rapidapi.com");
        ReflectionTestUtils.setField(dataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7e");
    }

    @Before
    public void setUp_invalidApiKey() {
        dataService = new YahooDataService();
        ReflectionTestUtils.setField(dataService, "HOST", "apidojo-yahoo-finance-v1.p.rapidapi.com");
        ReflectionTestUtils.setField(dataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7");
    }

    @Test
    public void getUrl_validHostParameters() {
        setUp_validApiKeyHostUrl();
        JSONObject json = dataService.getYahooMarketData("US");
        assertNotNull(json.toString());
    }

    @Test
    public void getData_invalidAPI() {
        setUp_invalidApiKey();
        JSONObject actual = dataService.getYahooWatchList();
        JSONObject expected = new JSONObject();
        expected.put("message", "You are not subscribed to this API.");
        assertEquals(expected.get("message"), actual.get("message"));
    }

}
